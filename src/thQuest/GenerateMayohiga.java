package thQuest;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.econ.EconomyAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.DerelictShipEntityPlugin;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.NebulaEditor;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.SalvageSpecialAssigner;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial;
import com.fs.starfarer.api.impl.campaign.terrain.BaseRingTerrain;
import com.fs.starfarer.api.impl.campaign.terrain.DebrisFieldTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.HyperspaceTerrainPlugin;
import com.fs.starfarer.api.util.Misc;
import thQuest.intel.ZetaIntel;

import java.awt.*;
import java.util.Random;

public class GenerateMayohiga {
    public static void createMayohiga(boolean inProgressGame){
        EconomyAPI globalEconomy = Global.getSector().getEconomy();
        StarSystemAPI system = Global.getSector().createStarSystem("Mayohiga");
        system.setBackgroundTextureFilename("graphics/backgrounds/background2.jpg");
        SectorEntityToken star = system.initStar("thQuestStarMayohiga","star_browndwarf",400f,-21000,-8000,150f);
        system.setLightColor(new Color(255,255,255));

        //chen's cats 800-1200
        system.addRingBand(star,"misc","rings_special0",256f,0,Color.white,256f,800f,60f);
        system.addRingBand(star,"misc","rings_special0",256f,1,Color.white,256f,900f,70f);
        system.addRingBand(star,"misc","rings_special0",256f,2,Color.white,256f,1000f,80f);
        system.addRingBand(star,"misc","rings_special0",256f,1,Color.white,256f,1100f,90f);
        SectorEntityToken ring = system.addTerrain(Terrain.RING, new BaseRingTerrain.RingParams(300 + 256, 1000f, null, "Chen's Cats"));
        ring.setCircularOrbit(star, 0, 0, 80);
        //orin's cats 1500-1800
        system.addRingBand(star,"misc","rings_special0",256f,0,Color.white,256f,1500f,130f);
        system.addRingBand(star,"misc","rings_dust0",256f,0,Color.white,256f,1600f,140f);
        system.addRingBand(star,"misc","rings_dust0",256f,1,Color.white,256f,1700f,150f);
        system.addRingBand(star,"misc","rings_asteroids0",256f,0,Color.white,256f,1800f,160f);
        system.addAsteroidBelt(star,100, 1800, 256f, 130f, 160f, Terrain.ASTEROID_BELT, "Orin's cart");
        SectorEntityToken ring2 = system.addTerrain(Terrain.RING, new BaseRingTerrain.RingParams(200 + 256, 1650f, null, "Orin's Cats"));
        ring2.setCircularOrbit(star, 0, 0, 150);
        //mike's cats 2300-3000
        system.addRingBand(star,"misc","rings_dust0",256f,0,Color.white,256f,2300f,200f);
        system.addRingBand(star,"misc","rings_dust0",256f,1,Color.white,256f,2400f,210f);
        system.addRingBand(star,"misc","rings_dust0",256f,2,Color.white,256f,2500f,220f);
        system.addRingBand(star,"misc","rings_dust0",256f,1,Color.white,256f,2600f,230f);
        system.addRingBand(star,"misc","rings_ice0",256f,0,Color.white,256f,2700f,240f);
        system.addRingBand(star,"misc","rings_ice0",256f,1,Color.white,256f,2800f,250f);
        system.addRingBand(star,"misc","rings_ice0",256f,2,Color.white,256f,2900f,260f);
        system.addRingBand(star,"misc","rings_asteroids0",256f,2,Color.white,256f,3000f,270f);

        SectorEntityToken ring3 = system.addTerrain(Terrain.RING, new BaseRingTerrain.RingParams(600 + 256, 2600f, null, "Mike's Cats"));
        ring3.setCircularOrbit(star, 0, 0, 230f);

        //hyperspace
        JumpPointAPI jumpPoint = Global.getFactory().createJumpPoint("thquest_mayohiga_jump_point","Mayohiga Jump-point");
        OrbitAPI orbit = Global.getFactory().createCircularOrbit(star,260f,1350,100f);
        jumpPoint.setOrbit(orbit);
        jumpPoint.setStandardWormholeToHyperspaceVisual();
        system.addEntity(jumpPoint);
        JumpPointAPI fringe = Global.getFactory().createJumpPoint("thquest_mayohiga_fringe_jump_point","Fringe Jump-point");
        OrbitAPI orbit2 = Global.getFactory().createCircularOrbit(star,10f,3300f,310f);
        fringe.setOrbit(orbit2);
        fringe.setStandardWormholeToHyperspaceVisual();
        system.addEntity(fringe);
        system.generateAnchorIfNeeded();
        system.autogenerateHyperspaceJumpPoints(true,false);
        //alpha
        SectorEntityToken alpha = system.addCustomEntity("thquest_habitat_alpha","Habitat Alpha","thquest_station_side01_ruin", "neutral");
        alpha.setCircularOrbitPointingDown(star,100,2000,180);
        Misc.setAbandonedStationMarket("thquest_alpha",alpha);
        alpha.setInteractionImage("illustrations", "abandoned_station");
        alpha.setCustomDescriptionId("thquest_habitat_alpha");
        //zeta
        SectorEntityToken zeta = system.addCustomEntity("thquest_habitat_zeta","Habitat Zeta","thquest_station_mining00", Factions.NEUTRAL);
        zeta.setCircularOrbitPointingDown(star,200,1300,100);
        Misc.setAbandonedStationMarket("thquest_zeta",zeta);
        zeta.setInteractionImage("illustrations", "abandoned_station2");
        zeta.setCustomDescriptionId("thquest_habitat_zeta");
        zeta.getMemory().set("$hasDefenders",true);
        //zeta fleet

            //CampaignFleetAPI fleet2 = (CampaignFleet) FleetFactory.createGenericFleet(Factions.DERELICT, "Habitat Zeta Defence Fleet", .45f, 50);
            //fleet2.setFaction(Factions.DERELICT);
            //FleetMemberAPI station = Global.getFactory().createFleetMember(FleetMemberType.SHIP, "station1_Standard");
            //station.setShipName("Habitat Zeta");
            // fleet2.addFleetMember(station);
            Random r = new Random();

           //for(FleetMemberAPI s:fleet2.getMembers()){
            //    DModManager.addDMods(s,false,Math.abs(r.nextInt())%4+2,r);

            //}

        //CampaignFleetAPI fleet2 = FleetFactoryV3.createEmptyFleet(Factions.DERELICT,FleetTypes.TASK_FORCE, null);
        //FleetFactoryV3.addCombatFleetPoints(fleet2,r,50,0,0,new FleetParamsV3(new Vector2f(0,0),Factions.DERELICT,0.45f,FleetTypes.TASK_FORCE,50,0,0,0,0,0,0.45f));
        CampaignFleetAPI fleet2 =FleetFactoryV3.createFleet(new FleetParamsV3(null,Factions.DERELICT,0.2f,FleetTypes.TASK_FORCE,50,0,0,0,0,0,0.2f));
            zeta.getMemory().set("$defenderFleet",fleet2);

        //iota(test)
        //SectorEntityToken iota = system.addCustomEntity("thquest_test","test","derelict_probe",Factions.DERELICT);
        //iota.setCircularOrbitPointingDown(zeta,0,100,100);
        //iota.getMemory().set("$hasDefenders",true);

        //globalEconomy.addMarket(zetaMarket,false);
        //zetaMarket.setUseStockpilesForShortages(true);
        //station kappa
        SectorEntityToken kappa = system.addCustomEntity("thquest_habitat_kappa","Habitat Kappa","station_side04", "thquest_gensokyo");
        kappa.setCircularOrbitPointingDown(star,0,1650,145);
        MarketAPI kappaMarket = Global.getFactory().createMarket("thquest_habitat_kappa","Habitat Kappa",4);
        kappa.setCustomDescriptionId("thquest_habitat_kappa");
        //kappaMarket.setPlanetConditionMarketOnly(false);//perhaps don't need this
        kappaMarket.setFactionId("thquest_gensokyo");
        kappaMarket.addCondition(Conditions.POPULATION_4);
        kappaMarket.addCondition(Conditions.RARE_ORE_ABUNDANT);
        kappaMarket.addCondition(Conditions.ORE_MODERATE);
        kappaMarket.getCondition(Conditions.RARE_ORE_ABUNDANT).setSurveyed(true);
        kappaMarket.getCondition(Conditions.ORE_MODERATE).setSurveyed(true);
        kappaMarket.setSize(4);
        kappaMarket.addSubmarket(Submarkets.SUBMARKET_OPEN);
        kappaMarket.addSubmarket(Submarkets.SUBMARKET_BLACK);
        kappaMarket.addSubmarket(Submarkets.SUBMARKET_STORAGE);
        kappaMarket.addIndustry(Industries.ORBITALWORKS);
        kappaMarket.addIndustry(Industries.PATROLHQ);
        kappaMarket.addIndustry(Industries.ORBITALSTATION_HIGH);
        kappaMarket.addIndustry(Industries.SPACEPORT);
        kappaMarket.addIndustry(Industries.MINING);
        kappaMarket.addIndustry(Industries.POPULATION);
        kappaMarket.getTariff().modifyFlat("default_tariff", kappaMarket.getFaction().getTariffFraction());
        kappaMarket.setSurveyLevel(MarketAPI.SurveyLevel.FULL);
        kappa.setMarket(kappaMarket);
        kappaMarket.setPrimaryEntity(kappa);
        if(inProgressGame){
            populateIntelBoard(kappa);
        }
        globalEconomy.addMarket(kappaMarket,false);
        //stable locations
        SectorEntityToken relay = system.addCustomEntity("thquest_mayohiga_relay","Mayohiga Relay", Entities.COMM_RELAY_MAKESHIFT,"thquest_gensokyo");
        relay.setCircularOrbitPointingDown(star,260f,4500f,400f);
        SectorEntityToken buoy = system.addCustomEntity("thquest_mayohiga_buoy","Mayohiga Buoy",Entities.NAV_BUOY_MAKESHIFT,"thquest_gensokyo");
        buoy.setCircularOrbit(star,87f,5000,600f);
        //debris
        DebrisFieldTerrainPlugin.DebrisFieldParams params = new DebrisFieldTerrainPlugin.DebrisFieldParams(
                500f, // field radius - should not go above 1000 for performance reasons
                -1f, // density, visual - affects number of debris pieces
                10000000f, // duration in days
                0f); // days the field will keep generating glowing pieces
        params.source = DebrisFieldTerrainPlugin.DebrisFieldSource.MIXED;
        params.baseSalvageXP = 250; // base XP for scavenging in field
        SectorEntityToken alphaDebris = Misc.addDebrisField(system, params, StarSystemGenerator.random);
        alphaDebris.setSensorProfile(null);
        alphaDebris.setDiscoverable(null);
        alphaDebris.setCircularOrbit(alpha, 0f, 1f, 60f);
        alphaDebris.setId("thquest_debris_1");

        SectorEntityToken wildDebris = Misc.addDebrisField(system,params, StarSystemGenerator.random);
        wildDebris.setCircularOrbit(star,StarSystemGenerator.random.nextInt()%360,8000f,1000f);
        wildDebris.setSensorProfile(300f);//I don't know what is a good number
        wildDebris.setDiscoverable(true);

        SectorEntityToken randomDebris = Misc.addDebrisField(system,params, StarSystemGenerator.random);
        randomDebris.setCircularOrbit(star,StarSystemGenerator.random.nextInt()%360,4000f,500f);
        randomDebris.setSensorProfile(300f);//I don't know what is a good number
        randomDebris.setDiscoverable(true);
        //hyperspace cleaning
        clearDeepHyper(system.getHyperspaceAnchor(),400);
        //derilicts
        addDerelict(system,alpha, "eagle_xiv_Elite", ShipRecoverySpecial.ShipCondition.AVERAGE,100f,false);
        addDerelict(system,alpha, "ox_Standard", ShipRecoverySpecial.ShipCondition.WRECKED,60f,true);
        addDerelict(system,alpha, "buffalo_luddic_church_Standard", ShipRecoverySpecial.ShipCondition.WRECKED,110f,false);
        addDerelict(system,alpha, "kite_pirates_Raider", ShipRecoverySpecial.ShipCondition.BATTERED,160f,true);
        addDerelict(system,alpha, "brawler_tritachyon_Standard", ShipRecoverySpecial.ShipCondition.WRECKED,110f,false);

        addDerelict(system,relay, "drover_Strike", ShipRecoverySpecial.ShipCondition.BATTERED,110f,false);
        addDerelict(system,relay, "wayfarer_Standard", ShipRecoverySpecial.ShipCondition.WRECKED,150f,false);
        addDerelict(system,relay, "tarsus_Standard", ShipRecoverySpecial.ShipCondition.WRECKED,220f,true);
        int yourCapitalShip =StarSystemGenerator.random.nextInt()%6;
        switch(yourCapitalShip){
            case 0:
                addDerelict(system, wildDebris, "conquest_Standard", ShipRecoverySpecial.ShipCondition.AVERAGE,0f,true);
            break;
            case 1:
                addDerelict(system, wildDebris, "legion_Escort", ShipRecoverySpecial.ShipCondition.AVERAGE,0f,true);
            break;
            case 2:
                addDerelict(system, wildDebris, "prometheus_Super", ShipRecoverySpecial.ShipCondition.AVERAGE,0f,true);
            break;
            case 3:
                addDerelict(system, wildDebris, "atlas_Standard", ShipRecoverySpecial.ShipCondition.AVERAGE,0f,true);
            break;
            case 4:
                addDerelict(system, wildDebris, "prometheus2_Super", ShipRecoverySpecial.ShipCondition.AVERAGE,0f,true);
            break;
            case 5:
                addDerelict(system, wildDebris, "atlas2_Standard", ShipRecoverySpecial.ShipCondition.AVERAGE,0f,true);
        }
        system.setEnteredByPlayer(true);//TODO ???
        addDerelict(system,star,"venture_Outdated", ShipRecoverySpecial.ShipCondition.WRECKED,5100f,false).addTag("thquestHoSZVenture");
    }
    protected static SectorEntityToken addDerelict(StarSystemAPI system, SectorEntityToken focus, String variantId,
                                      ShipRecoverySpecial.ShipCondition condition, float orbitRadius, boolean recoverable) {
        DerelictShipEntityPlugin.DerelictShipData params = new DerelictShipEntityPlugin.DerelictShipData(new ShipRecoverySpecial.PerShipData(variantId, condition, 0f), false);
        SectorEntityToken ship = BaseThemeGenerator.addSalvageEntity(system, Entities.WRECK, Factions.NEUTRAL, params);
        ship.setDiscoverable(true);

        float orbitDays = orbitRadius / (10f + (float) Math.random() * 5f);
        ship.setCircularOrbit(focus, (float) Math.random() * 360f, orbitRadius, orbitDays);

        if (recoverable) {
            SalvageSpecialAssigner.ShipRecoverySpecialCreator creator = new SalvageSpecialAssigner.ShipRecoverySpecialCreator(null, 0, 0, false, null, null);
            Misc.setSalvageSpecial(ship, creator.createSpecial(ship, null));
        }
        return ship;
    }
    public static void clearDeepHyper(SectorEntityToken entity, float radius) {
        // deep hyperspace removal (copypasted from UW)
        HyperspaceTerrainPlugin plugin = (HyperspaceTerrainPlugin) Misc.getHyperspaceTerrain().getPlugin();
        NebulaEditor editor = new NebulaEditor(plugin);

        float minRadius = plugin.getTileSize() * 2f;
        editor.clearArc(entity.getLocation().x, entity.getLocation().y, 0, radius + minRadius * 0.5f, 0, 360f);
        editor.clearArc(entity.getLocation().x, entity.getLocation().y, 0, radius + minRadius, 0, 360f, 0.25f);
    }
    public static void colonizeZeta(){
        Log.getLogger().info("Building Zeta");
        //Zeta colony
        SectorEntityToken zeta =Global.getSector().getEntityById("thquest_habitat_zeta");
        zeta.setFaction("thquest_gensokyo");
        zeta.setInteractionImage("illustrations","orbital");
        zeta.setCustomDescriptionId("thquest_habitat_zeta_plus");
        zeta.getMemory().set("$thquestZetaBuilding",false);
        MarketAPI zetaMarket = Global.getFactory().createMarket("thquest_zeta_market","Habitat Zeta",4);
        zetaMarket.setFactionId("thquest_gensokyo");
        zetaMarket.setPlanetConditionMarketOnly(false);//perhaps don't need this
        zetaMarket.addCondition(Conditions.POPULATION_4);
        zetaMarket.setSize(4);
        zetaMarket.addSubmarket(Submarkets.SUBMARKET_OPEN);
        zetaMarket.addSubmarket(Submarkets.SUBMARKET_BLACK);
        zetaMarket.addSubmarket(Submarkets.SUBMARKET_STORAGE);
        zetaMarket.addIndustry(Industries.LIGHTINDUSTRY);
        zetaMarket.addIndustry(Industries.REFINING);
        zetaMarket.addIndustry(Industries.PATROLHQ);
        zetaMarket.addIndustry(Industries.ORBITALSTATION);
        zetaMarket.addIndustry(Industries.SPACEPORT);
        zetaMarket.addIndustry(Industries.POPULATION);
        zetaMarket.getTariff().modifyFlat("default_tariff", zetaMarket.getFaction().getTariffFraction());
        zetaMarket.setSurveyLevel(MarketAPI.SurveyLevel.FULL);
        zeta.setMarket(zetaMarket);

        populateIntelBoard(zeta);

        zetaMarket.setPrimaryEntity(zeta);
        Global.getSector().getEconomy().addMarket(zetaMarket,true);
        //idk
        zetaMarket.getMemory().set("$abandonedStation",false);
        //intel item
        Global.getSector().getIntelManager().addIntel(new ZetaIntel(zeta,zeta));

    }
    //more or less copied from nex
    public static void populateIntelBoard(SectorEntityToken place){
        MarketAPI market =place.getMarket();
        PersonAPI person = place.getFaction().createRandomPerson();
        person.setPostId(Ranks.POST_ADMINISTRATOR);
        market.getCommDirectory().addPerson(person);

        person = place.getFaction().createRandomPerson();
        person.setPostId(Ranks.POST_PORTMASTER);
        market.getCommDirectory().addPerson(person);

        person = place.getFaction().createRandomPerson();
        person.setPostId(Ranks.POST_SUPPLY_OFFICER);
        market.getCommDirectory().addPerson(person);

    }
}
