package thQuest.intel;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.impl.campaign.intel.misc.BreadcrumbIntel;
import com.fs.starfarer.api.ui.SectorMapAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;

import java.util.HashSet;
import java.util.Set;

public class GensokyoPointer extends BreadcrumbIntel {
    public GensokyoPointer(SectorEntityToken foundAt, SectorEntityToken target) {
        super(foundAt, target);
    }
    @Override
    public String getName() {
        return "Gensokyo Discovered";
    }

    @Override
    public String getText() {
        return "Gensokyo Discovered";
    }

    @Override
    public String getIcon() {
        return Global.getSettings().getSpriteName("intel","new_planet_info");
    }

    @Override
    public boolean hasSmallDescription() {
        return true;
    }

    @Override
    public Set<String> getIntelTags(SectorMapAPI map) {
        HashSet<String> set= new HashSet<>();
        set.add("Fleet log");
        return set;
    }

    @Override
    public FactionAPI getFactionForUIColors() {
        return Global.getSector().getFaction("thquest_gensokyo");
    }

    @Override
    public void createSmallDescription(TooltipMakerAPI info, float width, float height) {
        info.addPara("You brought Yumemi and Chiyuri back home and got off of Gilead. " +
                "When you get back to your ship, your sensors officer tells you that your sensors have detected" +
                " a planet that shouldn't exist.",10);
        info.addButton("Delete", BUTTON_DELETE,300,20,20);
    }
}
