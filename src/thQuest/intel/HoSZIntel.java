package thQuest.intel;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.impl.campaign.intel.BaseIntelPlugin;
import com.fs.starfarer.api.ui.SectorMapAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;

import java.util.HashSet;
import java.util.Set;

public class HoSZIntel extends BaseIntelPlugin {
    String name="Recover Mercury Device";
    public HoSZIntel(){
    }
    public String getName() {
        return name;
    }

    @Override
    public SectorEntityToken getMapLocation(SectorMapAPI map){
        int stage = Integer.parseInt((String)Global.getSector().getMemory().get("$thquestHoSZ"));
        SectorEntityToken destination=null;
        switch(stage) {
            case 1:{
                destination=Global.getSector().getEntityById("thQuestStarMayohiga");
                break;
            }
            case 2:{
                destination=null;
                break;
            }
            case 3:{
                //TT?
                break;
            }
            case 4:{

                destination=null;
                break;
            }
            case 5:{

                //TT?
                break;
            }
            case 6:{

                destination=null;
                break;
            }
            default:{
                //shouldn't see this
                break;
            }
        }
        return destination;
    }

    @Override
    public String getIcon() {
        return Global.getSettings().getSpriteName("intel","thQuest_visit_object");
    }

    @Override
    public boolean hasSmallDescription() {
        return true;
    }

    @Override
    public Set<String> getIntelTags(SectorMapAPI map) {
        HashSet<String> set= new HashSet<>();
        set.add("Fleet log");
        return set;
    }

    @Override
    public FactionAPI getFactionForUIColors() {
        return Global.getSector().getFaction("neutral");
    }

    @Override
    public void createSmallDescription(TooltipMakerAPI info, float width, float height) {
        int stage = Integer.parseInt((String)Global.getSector().getMemory().get("$thquestHoSZ"));
        switch(stage){
            case 1:{
                info.addPara("Find clues about hyperspace youkai in the Mayohiga star system.",10);
                break;
            }
            case 2:{
                info.addPara("Find the device somewhere in the core worlds.",10);
                break;
            }
            case 5:{
                //if on a mission for Yukari mention that.
                info.addPara("You have the device in your posession.",10);
                info.addPara("Being as  dangerous as it is, no one on your ship is willing to use it, but some factions might be interested in this strange technology.",10);
                break;
            }
            case 6:{
                info.addPara("You have sold the device to Tri-Tachyon.",10);
                break;
            }
            case 7:{

                info.addPara("You have given the device to Galatia Academy.",10);
                break;
            }
            case 8:{

                info.addPara("You have sold the device to Gensokyo",10);
                break;
            }

            default:{
                info.addPara("You shouldn't be seeing this.",10);
                break;
            }
        }
    }

}
