package thQuest.intel;

import com.fs.graphics.A;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.comm.IntelInfoPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.intel.BaseIntelPlugin;
import com.fs.starfarer.api.ui.SectorMapAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class YumBHIntel extends BaseIntelPlugin {
    SectorEntityToken location;
    SectorEntityToken destination;
    Boolean yumFirst;
    Boolean alternateDescription=false;
    public YumBHIntel(SectorEntityToken location, SectorEntityToken destination, Boolean yumFirst) {
    this.location = location;
    this.destination = destination;
    this.yumFirst=yumFirst;
    this.important=true;
        Misc.makeImportant(destination,"ybh");
    }

    public void setDestination(SectorEntityToken destination) {
        Misc.makeUnimportant(this.destination,"ybh");
        this.destination = destination;
        Misc.makeImportant(destination,"ybh");
    }
    public void setAlternateDescription(boolean b){
        alternateDescription=b;
    }

    @Override
    public SectorEntityToken getMapLocation(SectorMapAPI map){
        return destination;
    }
    @Override
    public java.lang.String getSmallDescriptionTitle(){
     return "Yumemi back home";
    }
    @Override
    public java.lang.String getIcon(){
        if(yumFirst) {
            return Global.getSettings().getSpriteName("characters", "thQuest_yum");
        }else{
            return Global.getSettings().getSpriteName("characters", "thQuest_chi");
        }
    }

    @Override
    public void createSmallDescription(TooltipMakerAPI info,float width,float height){
        if(yumFirst) {
            info.addImage(Global.getSettings().getSpriteName("characters", "thQuest_yum"), width,128,10);
            if(alternateDescription){
                info.addPara("go to Gilead to help Yumemi get home!",10);
                addBulletPoints(info, ListInfoMode.IN_DESC);
            }else {
                info.addPara("go to Habitat Kappa, then go to Gilead to help Yumemi get home!",10);
                addBulletPoints(info, ListInfoMode.IN_DESC);
            }
            }else {
            info.addImage(Global.getSettings().getSpriteName("characters", "thQuest_chi"),width, 128,10);
            if (alternateDescription) {
                info.addPara("Go to Gilead to help Chiyuri and her friend get to Gensokyo!", 10);
                addBulletPoints(info, ListInfoMode.IN_DESC);
            } else {
                info.addPara("Go to Galatia academy to help Chiyuri get back to her friends!", 10);
            }
        }
    }

    @Override
    protected void addBulletPoints(TooltipMakerAPI info, ListInfoMode mode) {
        super.addBulletPoints(info, mode);
        Color h=Misc.getHighlightColor();
        Color tc = getBulletColorForMode(mode);
        info.addPara("  -%s reward", 10, tc, h, Misc.getDGSCredits(10000));

    }

    @Override
    public java.util.Set<java.lang.String> getIntelTags(SectorMapAPI map){
        Set<String> s = new HashSet<String>();
        s.add("Story");
        return s;
    }
    @Override
    public boolean hasSmallDescription(){
    return true;
    }
    @Override
    public void createIntelInfo(TooltipMakerAPI info, IntelInfoPlugin.ListInfoMode mode){
        info.addPara("Yumemi Back home", Color.PINK,10);
        if(yumFirst) {
            if(alternateDescription){
                info.addPara("Go to Gilead", 10);
            }else {
                info.addPara("Go to outpost Kappa", 10);
            }
            }else {
            if (alternateDescription) {
                info.addPara("Go to Gilead", 10);
            } else {
                info.addPara("Go to Galatia Academy", 10);
            }
        }
    }
    @Override
    public void endImmediately() {
        Misc.makeUnimportant(this.destination,"ybh");
        super.endImmediately();
    }
}
