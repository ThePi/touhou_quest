package thQuest;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.loading.Description;
import com.fs.starfarer.api.util.Misc;

import java.awt.*;

public class GenerateGensokyo {
    public static void createGensokyo(){
        StarSystemAPI s= Global.getSector().getStarSystem("Canaan");
        float angle = s.getEntityById("Gilead").getCircularOrbitAngle()+180;
        if(angle>360){
            angle=angle-360;
        }
        PlanetAPI g =s.addPlanet("thquest_gensokyo", s.getStar(), "Gensokyo", "terran", angle, 190, 5000, 250);
        Misc.initConditionMarket(g);//may or may not be necessary
        g.getSpec().setPitch( 190.0f);
        g.getSpec().setPlanetColor(new Color(255,245,225,255));
        g.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "volturn"));
        g.getSpec().setGlowColor(new Color(250,225,195,255));
        g.getSpec().setUseReverseLightForGlow(true);
        g.applySpecChanges();//must change specs to use, must be before anything else
        g.setCustomDescriptionId("thquestgensokyo");
        g.setFaction("thquest_gensokyo");
        MarketAPI gm =Global.getFactory().createMarket("thquest_gensokyo_market",g.getName(),3);
        gm.setFactionId("thquest_gensokyo");
        gm.setPlanetConditionMarketOnly(false);//needs to be true to be uninhabited
        gm.addCondition(Conditions.TERRAN);
        gm.addCondition(Conditions.HABITABLE);
        gm.addCondition(Conditions.MILD_CLIMATE);
        gm.addCondition(Conditions.ORE_MODERATE);
        gm.addCondition(Conditions.FARMLAND_RICH);
        gm.addCondition(Conditions.ORGANICS_ABUNDANT);
        gm.setPrimaryEntity(g);
        g.setMarket(gm);//needs to be both ways
        gm.setSurveyLevel(MarketAPI.SurveyLevel.SEEN);

        //setup the moon
        PlanetAPI m = s.addPlanet("thquest_moon",g,"The moon","barren", 0, 50, 500, 30);
        m.getSpec().setTexture(Global.getSettings().getSpriteName("planets", "barren03"));
        m.getSpec().setPlanetColor(new Color(235,255,245,255));
        m.getSpec().setPitch( 140.0f);
        m.applySpecChanges();
        MarketAPI mm =Global.getFactory().createMarket("thquest_moon_market",m.getName(),0);
        mm.setPlanetConditionMarketOnly(true);
        mm.setFactionId(Factions.NEUTRAL);
        mm.addCondition(Conditions.NO_ATMOSPHERE);
        mm.addCondition(Conditions.LOW_GRAVITY);
        mm.addCondition(Conditions.ORE_ULTRARICH);
        mm.addCondition(Conditions.RARE_ORE_ULTRARICH);
        mm.addCondition(Conditions.ORGANICS_ABUNDANT);
        mm.setPrimaryEntity(m);
        m.setMarket(mm);
        mm.setSurveyLevel(MarketAPI.SurveyLevel.SEEN);
        //gm.reapplyConditions();
        //Misc.initConditionMarket(g);
        //s.updateAllOrbits();//updates map
    }
}
