package thQuest;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.characters.FullName;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Ranks;
import com.fs.starfarer.api.impl.combat.sanaeMissileAI;
import exerelin.campaign.SectorManager;

public class Plugin extends BaseModPlugin {
    @Override
    public void onGameLoad(boolean newGame) {
        Log.getLogger().info("thQuest loaded");
    }

    @Override
    public void onNewGame() {
        //script setup
        Global.getSector().getListenerManager().addListener(new ThquestEventListener(true));
        //faction setup
        Global.getSector().getFaction("thquest_gensokyo").setRelationship(Factions.PIRATES,-0.50f);
        Global.getSector().getFaction("thquest_gensokyo").setRelationship(Factions.LUDDIC_PATH,-0.50f);
        //person setup

        //Nex compatibility setting, if there is no nex or corvus mode(Nex), generate system and quest stuff
        boolean haveNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        if (!haveNexerelin || SectorManager.getManager().isCorvusMode()) {
            setup(false);
        }
    }

    @Override
    public void onEnabled(boolean wasEnabledBefore){
        if(!wasEnabledBefore){
            //script setup
            Global.getSector().getListenerManager().addListener(new ThquestEventListener(true));
            //faction setup
            Global.getSector().getFaction("thquest_gensokyo").setRelationship(Factions.PIRATES,-0.50f);
            Global.getSector().getFaction("thquest_gensokyo").setRelationship(Factions.LUDDIC_PATH,-0.50f);
            //person setup

            //Nex compatibility setting, if there is no nex or corvus mode(Nex), generate system and quest stuff
            boolean haveNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
            if (!haveNexerelin || SectorManager.getManager().isCorvusMode()) {
                setup(true);
            }
        }
        Global.getSector().getMemory().set("$touhouquestybh","0");


    }
    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip){
        if(missile.getProjectileSpecId().equals("thquestSnake")){
            return new PluginPick<MissileAIPlugin>(new sanaeMissileAI(missile,launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
        }
        return null;
    }
    public void setup(boolean inProgressGame){
        //System.out.println("SETUP CALLED THQUEST");
        PersonAPI yum = Global.getFactory().createPerson();
        yum.setId("thquestYum");
        yum.setName(new FullName("Yumemi","Okazaki", FullName.Gender.FEMALE));
        yum.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_yum"));
        yum.setRankId(Ranks.CITIZEN);
        yum.setFaction(Factions.NEUTRAL);
        yum.setPostId(Ranks.POST_SCIENTIST);
        SectorEntityToken s =Global.getSector().getStarSystem("galatia").getEntityById("station_galatia_academy");
        Global.getSector().getImportantPeople().addPerson(yum);
        //Yes, I can't pass in yum object
        s.getMarket().getCommDirectory().addPerson(Global.getSector().getImportantPeople().getPerson("thquestYum"));

        PersonAPI chi = Global.getFactory().createPerson();
        chi.setId("thquestChi");
        chi.setName(new FullName("Chiyuri","Kitashirakawa", FullName.Gender.FEMALE));
        chi.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_chi"));
        chi.setRankId(Ranks.CITIZEN);
        chi.setFaction(Factions.NEUTRAL);
        chi.setPostId(Ranks.POST_SPACER);
        Global.getSector().getImportantPeople().addPerson(chi);

        PersonAPI rei = Global.getFactory().createPerson();
        rei.setId("thquestRei");
        rei.setName(new FullName("Reimu","Hakurei", FullName.Gender.FEMALE));
        rei.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_rei"));
        rei.setRankId(Ranks.CITIZEN);
        rei.setFaction("thquest_gensokyo");
        rei.setPostId(Ranks.POST_CITIZEN);
        Global.getSector().getImportantPeople().addPerson(rei);


        PersonAPI yuk = Global.getFactory().createPerson();
        yuk.setId("thquestYuk");
        yuk.setName(new FullName("Yukari","Yakumo", FullName.Gender.FEMALE));
        yuk.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_yuk"));
        //need to add sage rank
        yuk.setRankId(Ranks.CITIZEN);
        yuk.setFaction("thquest_gensokyo");
        yuk.setPostId(Ranks.POST_CITIZEN);
        Global.getSector().getImportantPeople().addPerson(yuk);

        //create star system
        GenerateMayohiga.createMayohiga(inProgressGame);
    }
}
