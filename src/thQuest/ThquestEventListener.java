package thQuest;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BaseCampaignEventListener;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Industries;
import com.fs.starfarer.api.impl.campaign.ids.Ranks;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;

public class ThquestEventListener extends BaseCampaignEventListener {
    Long initTime;
    Boolean zetaDone;
    public ThquestEventListener(boolean permaRegister) {
        super(permaRegister);
        zetaDone=false;
        initTime=null;
    }
    @Override
    public void reportEconomyTick(int interIndex){
        //Log.getLogger().info("TICK");
        if(initTime!=null){
            if(Global.getSector().getClock().getElapsedDaysSince(initTime)>8&&!zetaDone){
                zetaDone=true;
                GenerateMayohiga.colonizeZeta();
                initTime=null;
            }
        }
    }
    public void startZetaClock(){
        initTime=Global.getSector().getClock().getTimestamp();
    }
}
