package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.comm.IntelInfoPlugin;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;
import thQuest.intel.YumBHIntel;

import java.util.List;
import java.util.Map;
//this is stage 2. regardless of where it actually is...
public class ThquestYbhKappa implements CommandPlugin {
    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        List<IntelInfoPlugin> i = Global.getSector().getIntelManager().getIntel();
        for(IntelInfoPlugin a:i){
            if(a.getClass()== YumBHIntel.class){
                ((YumBHIntel) a).setDestination(Global.getSector().getEntityById("Gilead"));
                ((YumBHIntel) a).setAlternateDescription(true);
            }
        }

        return false;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        return 0;
    }
}
