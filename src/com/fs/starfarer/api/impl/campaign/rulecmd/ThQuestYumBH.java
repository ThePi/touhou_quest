package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.comm.IntelManagerAPI;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.intel.BaseIntelPlugin;
import com.fs.starfarer.api.util.Misc;
import thQuest.intel.YumBHIntel;

import java.util.List;
import java.util.Map;

//this is stage 1 regardless of where it starts...
public class ThQuestYumBH implements CommandPlugin {
    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        IntelManagerAPI i= Global.getSector().getIntelManager();
        BaseIntelPlugin b;
        //this should remove yumemi from intel board
        Global.getSector().getEntityById("station_galatia_academy").getMarket().getCommDirectory().removePerson(Global.getSector().getImportantPeople().getPerson("thquestYum"));
        //this will change the quest based on where it was accepted.
        String option=null;

        if(memoryMap.get(MemKeys.LOCAL).get("$id").equals("thquestYum")){
            b = new YumBHIntel(Global.getSector().getEntityById("station_galatia_academy"),Global.getSector().getEntityById("thquest_habitat_kappa"),true);
            Global.getSector().getMemory().set("$touhouquestybh","1");
        }else{
            b = new YumBHIntel(Global.getSector().getEntityById("thquest_habitat_kappa"),Global.getSector().getEntityById("station_galatia_academy"),false);
            Global.getSector().getMemory().set("$touhouquestybh","2");
        }
        i.addIntel(b);
        if(memoryMap.get(MemKeys.LOCAL).get("$id").equals("thquestYum")) {
            dialog.dismiss();
        }
        return false;
    }
    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        return 0;
    }
}
