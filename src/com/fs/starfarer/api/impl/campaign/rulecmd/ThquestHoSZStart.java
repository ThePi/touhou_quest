package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.comm.IntelInfoPlugin;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;
import thQuest.intel.HoSZIntel;

import java.util.List;
import java.util.Map;

public class ThquestHoSZStart implements CommandPlugin {
    @Override
    public boolean execute(String s, InteractionDialogAPI interactionDialogAPI, List<Misc.Token> list, Map<String, MemoryAPI> map) {
        List<IntelInfoPlugin> i = Global.getSector().getIntelManager().getIntel();
        IntelInfoPlugin hosz=null;
        for(IntelInfoPlugin a:i){
            if(a.getClass()== HoSZIntel.class){
                hosz=a;
            }
        }
        if(hosz==null){
            //create HoSZ intel. It is possible to get it multiple ways
            Global.getSector().getIntelManager().addIntel(new HoSZIntel());
        }
        return true;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> list, Map<String, MemoryAPI> map) {
        return 0;
    }
}
