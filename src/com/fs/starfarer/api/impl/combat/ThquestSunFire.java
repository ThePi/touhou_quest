package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnFireEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;

public class ThquestSunFire implements OnFireEffectPlugin {
    @Override
    public void onFire(DamagingProjectileAPI projectile, WeaponAPI weapon, CombatEngineAPI engine) {
    //engine.spawnProjectile(weapon.getShip(),weapon, "thquest_sakuya_orb_medium", weapon.getFirePoint(0), weapon.getCurrAngle()+45, weapon.getShip().getVelocity());
        ThquestSunPlugin x=(ThquestSunPlugin)weapon.getEffectPlugin();
        x.startScript();
    }
}
