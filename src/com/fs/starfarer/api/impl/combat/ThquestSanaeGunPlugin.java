package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.campaign.BattleAPI;
import com.fs.starfarer.api.combat.*;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;

import java.util.ArrayList;
import java.util.List;

public class ThquestSanaeGunPlugin implements EveryFrameWeaponEffectPlugin {
    List<DamagingProjectileAPI> projList = new ArrayList<>();
    CombatEntityAPI target=null;
    @Override
    public void advance(float v, CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI) {
    //target = weaponAPI.getShip().getShipTarget();
    for(DamagingProjectileAPI d:projList){
        if(d.isExpired()){
            projList.remove(d);
        }
        if(d.getCustomData().get("thQuestTarget")==null){
            //read ship target
            target=weaponAPI.getShip().getShipTarget();
            if(target==null||target.getOwner()!=1){
                //if not valid, pick nearest enemy
                target= AIUtils.getNearestEnemy(d);
            }
            d.setCustomData("thQuestTarget",target);
        }
    }
    }
    public void addProj(DamagingProjectileAPI damagingProjectileAPI){
        projList.add(damagingProjectileAPI);
    }
}
