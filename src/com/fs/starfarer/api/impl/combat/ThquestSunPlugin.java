package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.input.InputEventAPI;
import org.lwjgl.util.vector.Vector2f;
import thQuest.Log;

import java.util.List;

public class ThquestSunPlugin implements EveryFrameWeaponEffectPlugin {
    float countdown=0;
    float angle = 0;
    float carry =0;
    Vector2f b1=new Vector2f(0,0);
    Vector2f b2=new Vector2f(0,0);
    Vector2f b3=new Vector2f(0,0);

    public void startScript(){
        countdown=2;
    }
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        if(countdown>0){
            countdown=countdown-amount;
            Vector2f speed=weapon.getShip().getVelocity();
            while(amount+carry>.002){
                b1.set((float) (speed.x+(Math.random()-.5)*30), (float) (speed.y+(Math.random()-.5)*30));
                b2.set((float) (speed.x+(Math.random()-.5)*30), (float) (speed.y+(Math.random()-.5)*30));
                b3.set((float) (speed.x+(Math.random()-.5)*30), (float) (speed.y+(Math.random()-.5)*30));
                engine.spawnProjectile(weapon.getShip(), weapon,"thquest_blue_medium",weapon.getFirePoint(0),countdown*180+(float) Math.random()*15,b1);
                engine.spawnProjectile(weapon.getShip(), weapon,"thquest_blue_medium",weapon.getFirePoint(0),countdown*180+120+(float) Math.random()*15,b2);
                engine.spawnProjectile(weapon.getShip(), weapon,"thquest_blue_medium",weapon.getFirePoint(0),countdown*180+240+(float) Math.random()*15,b3);
                Global.getSoundPlayer().playSound("thquest_tan00",1,.2f,weapon.getFirePoint(0),speed);
                amount= (float) (amount-.002);
            }
            carry=amount+carry;
        }
    }
}
