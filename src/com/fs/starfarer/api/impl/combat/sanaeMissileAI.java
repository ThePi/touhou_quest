package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.*;
import com.sun.javafx.tk.quantum.LazyFonts;
import org.lazywizard.lazylib.LazyLib;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;
import thQuest.Log;

import javax.swing.*;

public class sanaeMissileAI implements MissileAIPlugin, GuidedMissileAI {
    MissileAPI missile;
    ShipAPI launchingShip;
    CombatEntityAPI target;
    Boolean secondStage=false;
    float setAngle;
    public sanaeMissileAI(MissileAPI missile, ShipAPI launchingShip){
    this.missile=missile;
    this.launchingShip=launchingShip;
    }
    @Override
    public void advance(float amount) {
        if(missile==null){
            return;
        }
        missile.giveCommand(ShipCommand.ACCELERATE);
        if(target==null){
            target=launchingShip.getShipTarget();
            if(target==null||target.getOwner()!=1){
                target= AIUtils.getNearestEnemy(missile);
                if(target==null){
                    //testing this
                    if(secondStage){
                        missile.setAngularVelocity(0);
                        float facing = missile.getFacing();
                        if(facing<setAngle||facing>setAngle){
                            missile.explode();
                        }

                    }
                    return;
                }
            }
        }
        if(target.getOwner()!=1){
            target=null;
        }
        if(secondStage){
            missile.setAngularVelocity(0);
            float facing = missile.getFacing();
            if(facing<setAngle||facing>setAngle){
                missile.explode();
            }
            return;
        }
        if(missile.getLocation()==null){
            return;
        }
        //this fixed a crash WHYYYYYY TARGET SHOULD NOT BE NULL!!!!!????!?!?!!?
        if(target==null||target.getLocation()==null){
            return;
        }
        double angle=VectorUtils.getAngle(missile.getLocation(),target.getLocation());
        //Log.getLogger().info(missile.getFacing());
        Vector2f v=VectorUtils.getDirectionalVector(missile.getLocation(),target.getLocation());
        //VectorUtils.

    missile.giveCommand(ShipCommand.ACCELERATE);
    float leftRange1 = missile.getFacing()-95;
    float leftRange2 = missile.getFacing()-85;
    if(leftRange1<0){
        leftRange1=360+leftRange1;
    }
    if(leftRange2<0){
        leftRange2=360+leftRange2;
    }
    float rightRange1 = missile.getFacing()+95;
    float rightRange2 = missile.getFacing()+85;
    if(rightRange1>360){
        rightRange1=rightRange1-360;
    }
    if(rightRange2>360){
        rightRange2=rightRange2-360;
    }

    if((angle>leftRange1&&angle<leftRange2)||(leftRange1>350&&angle>leftRange1)){
        missile.setFacing((float) (angle));
        missile.setAngularVelocity(0);
        //missile.giveCommand(ShipCommand.ACCELERATE);
        setAngle= (float) angle;
        secondStage=true;
        //Log.getLogger().info("angle detected! missile should go right!");
    }
    else if((angle<rightRange1&&angle>rightRange2)||(rightRange1<10&&angle<rightRange1)){
        missile.setFacing((float) (angle));
        missile.setAngularVelocity(0);
        //missile.giveCommand(ShipCommand.ACCELERATE);
        setAngle= (float) angle;
        secondStage=true;
        //Log.getLogger().info("angle detected! missile should go left!");

    } else if (missile.isFizzling()) {
        secondStage=true;
    }
    }

    @Override
    public CombatEntityAPI getTarget() {
        return target;
    }

    @Override
    public void setTarget(CombatEntityAPI target) {
    this.target=target;
    }
}
