package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.*;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

import java.util.ArrayList;
import java.util.List;


public class ThquestYoumuNonFire implements OnFireEffectPlugin, EveryFrameWeaponEffectPlugin {
    float speed=.1f;
    float wait=1.5f;
    int cycle = 3;
    int hits =0;
    Vector2f shipspeed;
    Vector2f postion;
    Float angle;
    Float carry=0f;
    @Override
    public void advance(float v, CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI) {
        if (cycle < 3) {
            if(cycle==1){
                //stall
                if(carry<wait){
                    carry=carry+v;
                    return;
                }else{
                    cycle++;
                    carry=carry-wait;
                    //update values
                    shipspeed=weaponAPI.getShip().getVelocity();
                    postion=weaponAPI.getFirePoint(0);
                    angle = weaponAPI.getCurrAngle();
                    return;
                }
            }
            while (v + carry >= speed) {
                carry=carry-speed;
                if (hits % 2 == 0) {
                    //spawn on right
                    spawnProjSeries(true, MathUtils.getPointOnCircumference(postion, 150f, (float) (angle + 5.625 * hits)), angle, combatEngineAPI, weaponAPI);
                } else {
                    //spawn on left
                    spawnProjSeries(false, MathUtils.getPointOnCircumference(postion, 150f, (float) (angle + (360 - 5.625 * (hits + 1)))), angle, combatEngineAPI, weaponAPI);
                }
                hits++;
                if(hits >31){
                    hits=0;
                    cycle++;
                }
            }
            carry=carry+v;
        }
    }

    @Override
    public void onFire(DamagingProjectileAPI damagingProjectileAPI, WeaponAPI weaponAPI, CombatEngineAPI combatEngineAPI) {
    cycle=0;
    shipspeed=weaponAPI.getShip().getVelocity();
    postion=weaponAPI.getFirePoint(0);
    angle = weaponAPI.getCurrAngle();
    }
    public void spawnProjSeries(boolean right,Vector2f pos,float angle,CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI){
        //1, 4, 7, 9, 11,13,13,13
        List<Float> offsets= new ArrayList<Float>();
        switch (hits){
            case 0:
            case 1:
                //1
                offsets.add(0f);
                break;
            case 2:
            case 3:
                offsets.add(10f);
                offsets.add(30f);
                offsets.add(-10f);
                offsets.add(-30f);
                //4
                break;
            case 4:
            case 5:
                offsets.add(0f);
                offsets.add(15f);
                offsets.add(30f);
                offsets.add(45f);
                offsets.add(-15f);
                offsets.add(-30f);
                offsets.add(-45f);

                //7
                break;
            case 6:
            case 7:
                offsets.add(0f);
                offsets.add(15f);
                offsets.add(30f);
                offsets.add(45f);
                offsets.add(60f);
                offsets.add(-15f);
                offsets.add(-30f);
                offsets.add(-45f);
                offsets.add(-60f);
                //9
                break;
            case 8:
            case 9:
                offsets.add(0f);
                offsets.add(15f);
                offsets.add(30f);
                offsets.add(45f);
                offsets.add(60f);
                offsets.add(75f);
                offsets.add(-15f);
                offsets.add(-30f);
                offsets.add(-45f);
                offsets.add(-60f);
                offsets.add(-75f);
                //11
                break;
            default:
                offsets.add(0f);
                offsets.add(15f);
                offsets.add(30f);
                offsets.add(45f);
                offsets.add(60f);
                offsets.add(75f);
                offsets.add(90f);
                offsets.add(-15f);
                offsets.add(-30f);
                offsets.add(-45f);
                offsets.add(-60f);
                offsets.add(-75f);
                offsets.add(-90f);
                //13
        }
for(Float x:offsets){
    spawnProj(right,pos,x+angle,combatEngineAPI,weaponAPI);
}
    }
    public void spawnProj(boolean right,Vector2f pos,float angle,CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI) {
    String type = "thquest_youmu_non_fake_0";
        if (right) {
            if (cycle == 0) {
            type="thquest_youmu_non_fake_1";
            } else if (cycle == 2) {
                type="thquest_youmu_non_fake_2";
            }
        } else {
            if (cycle == 0) {
                type = "thquest_youmu_non_fake_0";
            } else if (cycle == 2) {
                type = "thquest_youmu_non_fake_3";
            }
        }
            combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI, type, pos, angle, shipspeed);

}
}
