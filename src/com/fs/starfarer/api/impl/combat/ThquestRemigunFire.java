package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnFireEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;
import org.lwjgl.util.vector.Vector2f;

public class ThquestRemigunFire implements OnFireEffectPlugin {
    @Override
    public void onFire(DamagingProjectileAPI projectile, WeaponAPI weapon, CombatEngineAPI engine) {
        Vector2f shipSpeed=weapon.getShip().getVelocity();
        Vector2f projSpeed= new Vector2f(0,0);
        float angle = 0;
        //bigger bullets
        for(int i=0;i<8;i++){
            angle= (float) ((Math.random()-.5)*10);
            projSpeed.set((float) (shipSpeed.x+(Math.random()-.5)*30), (float) (shipSpeed.y+(Math.random()-.5)*30));
            engine.spawnProjectile(weapon.getShip(), weapon,"thquest_remi_gun_medium_red",weapon.getFirePoint(0), weapon.getCurrAngle()+angle,projSpeed);
        }
        //smaller bullets
        for(int i=0;i<20;i++){
            angle= (float) ((Math.random()-.5)*30);
            projSpeed.set((float) (shipSpeed.x+(Math.random()-.5)*250), (float) (shipSpeed.y+(Math.random()-.5)*250));
            engine.spawnProjectile(weapon.getShip(), weapon,"thquest_remi_gun_small_red",weapon.getFirePoint(0),weapon.getCurrAngle()+angle,projSpeed);
        }

    }
}
