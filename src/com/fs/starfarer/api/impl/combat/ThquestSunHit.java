package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import com.fs.starfarer.api.loading.DamagingExplosionSpec;
import com.fs.starfarer.api.loading.ProjectileSpecAPI;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class ThquestSunHit implements OnHitEffectPlugin {
    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
            //CombatEntityAPI x=engine.spawnProjectile(projectile.getSource(),projectile.getWeapon(),projectile.getWeapon().getId(),point,projectile.getAngularVelocity(),null);
        Color theColor = new Color(255,225,225,100);
        engine.spawnDamagingExplosion(new DamagingExplosionSpec(
                    3,300,100,3000,500,
                    CollisionClass.PROJECTILE_FF,CollisionClass.PROJECTILE_FIGHTER,5,5,
                    4,40, theColor,theColor),projectile.getSource(),projectile.getLocation());
    }
}
