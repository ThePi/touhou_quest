package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnFireEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;

public class ThquestBowlingFire implements OnFireEffectPlugin {
    @Override
    public void onFire(DamagingProjectileAPI damagingProjectileAPI, WeaponAPI weaponAPI, CombatEngineAPI combatEngineAPI) {
        ThquestBowlingPlugin x=(ThquestBowlingPlugin) weaponAPI.getEffectPlugin();
        x.startScript(weaponAPI.getFirePoint(0),weaponAPI.getCurrAngle(),weaponAPI.getShip().getVelocity());
    }
}
