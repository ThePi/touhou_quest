package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;
import org.lwjgl.util.vector.Vector2f;
import thQuest.Log;

public class ThquestBowlingPlugin implements EveryFrameWeaponEffectPlugin {
    public void startScript(Vector2f x,float y,Vector2f z){
        countdown=.075f;
        stage=2;
        fireLocation=x;
        fireAngle=y;
        shipSpeed=z;
    }
    float countdown=0;
    int stage=999;
    Vector2f fireLocation;
    Vector2f shipSpeed;
    float fireAngle;
    @Override
    public void advance(float amount, CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI) {

if(stage <6){
    countdown=countdown-amount;
    if(countdown<=0){
        countdown=countdown+.075f;
        int current=stage;
        while(current>stage*-1){
            combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_bowling",fireLocation,fireAngle+current*.5f,shipSpeed);
            Global.getSoundPlayer().playSound("thquest_tan00",1,1f,fireLocation,shipSpeed);
            current=current-2;
        }
        stage++;
    }

}
    }
}
